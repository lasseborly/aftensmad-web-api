var Sequelize = require('sequelize');
var database = require('../database/config.js');

var User = database.define('user', {
    name: {
        type: Sequelize.TEXT   
    },
    email: {
        type: Sequelize.TEXT
    }
}, {
  freezeTableName: true // Model tableName will be the same as the model name
});

User.sync({force: true}).then(function () {
  
    User.create({
    name: 'Peter Jensen',
    email: 'peterjensen@gmail.com'  
  });
    
    
  return User.create({
    name: 'Lasse Borly',
    email: 'lasseborly@gmail.com'  
  });
});

module.exports = User;