var Sequelize = require('sequelize');
var database = require('../database/config.js');

var Recipe = database.define('recipe', {
    title: {
        type: Sequelize.TEXT   
    },
    production_time: {
        type: Sequelize.INTEGER
    },
    cooking_time: {
        type: Sequelize.INTEGER
    },
    tip: {
        type: Sequelize.TEXT
    }
}, {
  freezeTableName: true // Model tableName will be the same as the model name
});

Recipe.sync({force: true}).then(function () {
  // Table created
  return Recipe.create({
    title: 'Burger',
    production_time: 20,
    cooking_time: 10,
    tip: 'Remember to cook them fully.'  
  });
});

module.exports = Recipe;