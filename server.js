// Module requires
var restify = require('restify');

// Initializes the server
var server = restify.createServer({
   name: 'aftensmadAPI',
});

//Allows post actions to obtain body data
server.use(restify.bodyParser());
//Allows for querys to be parsed
server.use(restify.queryParser());

// Imports the recipe routes and parses the server variable
require('./routes/recipe.js')(server);
require('./routes/user.js')(server);

// Starts the server on port 80
server.listen(80, function(){
	console.log("Server is listening");
});
