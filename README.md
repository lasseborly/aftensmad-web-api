#aftensmadAPI

######"aftensmadAPI" is the underlying API that is going to serve the eco-system Aftensmad. 

The API will be able to serve recipies and manage user credentials utilizing JSON as the transport format.

##Technology

* ###[Node.js](https://nodejs.org/)
     
    Used as the runtime envoirement for our JavaScript codebase.   
***

* ###[Restify](http://mcavage.me/node-restify/)

    Restify is build upon the same design as [Express.js](http://expressjs.com/) but is heavily shaven down and does not provide support for template engines etc. Restify is built specifically for REST web services.
***

* ###[Sequelize](http://docs.sequelizejs.com/en/latest/)

    A very popular ORM for node that supports PostgreSQL, MySQL, MariaDB, SQLite and MSSQL. In development I have gone with SQLite3 but future plans are to switch to MariaDB with reasoning in the missing scalabilty of SQLite.
***

* ###[SQLite3](https://github.com/mapbox/node-sqlite3)

    This module serves as the binding and driver for the Sequalize ORM when using SQLite.
***