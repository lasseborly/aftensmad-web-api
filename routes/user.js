var User = require('../models/User.js');

module.exports = function(server) {

    server.get('/api/user', findUsers);
    
    
}

function findUsers(req, res, next) {
    
    //If any search parameters have been used
    if(Object.keys(req.query).length != 0) {
        
        var queries = [];
        for (var key in req.query) {
            //console.log(key + req.query[key]);
            var obj = {};
            obj[key] = {$like: '%' + req.query[key] + '%'};
            queries.push(obj); 
        }
        console.log(queries);
        
        User.findAll({
        where: {
            $and: queries
        }
        }).then(function(users) {
        
            if(users.length != 0) {
                res.send(200, users);  
            }
            else {
                res.send(404);
            }
                      
        });
    }
    else {
        
        User.all().then(function(users) {
            
            
          
            if(users.length != 0) {
                res.send(200, users);       
            }
            else {
                res.send(404);
            }
            
        })
        
    }
    
    
    
}
                 
