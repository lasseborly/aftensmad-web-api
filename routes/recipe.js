var Recipe = require('../models/Recipe.js');

module.exports = function(server) {

  server.get('/recipe', findRecepies);

  function findRecepies(req, res, next) {

    Recipe.all().then(
      function(recipes) {

        if(recipes) {
            res.send(200, recipes);
        }
        else {
            res.send(404);
        }

      }
    )

  }



    //Returns a JSON of a single recipe by id
    server.get('/api/recipe/:id', function(req, res, next) {

        Recipe.findById(req.params.id).then(function(recipe) {

            if(recipe) {
                res.send(200, recipe);
            }
            else {
                res.send(404);
            }

        })

    });

    //Creates a new recipe
    server.post('/api/recipe', function(req, res, next) {

        var recipe = { title: req.body.title };

        Recipe.create(recipe).then(function() {
              res.send(201)
        })

    });

    //Updates a recipe by id
    server.put('/api/recipe/:id', function(req, res, next) {

        Recipe.findById(req.params.id).then(function(recipe) {

            if(recipe) {

                //Virker ikke.....
                recipe.title = (req.body.title !== undefined ) ? req.body.title : recipe.title;

                recipe.save();
                res.send(200, recipe);


            }
            else {
                res.send(404);
            }

        })

    });

    //Deletes a recipe by id
    server.del('/api/recipe/:id', function(req, res, next) {

        Recipe.findById(req.params.id).then(function(recipe) {

            if(recipe) {
                recipe.destroy();
                res.send(204);
            }
            else {
                res.send(404);
            }

        })

    });


};
